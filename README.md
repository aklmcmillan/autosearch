# Automated AFSA literature search README

This tool uses the selenium web scraping package in lieu of an API for batch
searching on the AFSA database website. With a given search keyword, the tool
will output the number of search results. This tool operates under 2 assumptions:
1. You are connected to a network with an AFSA account
2. the html tag IDs have not changed on the AFSA search portal

If the tool is consistently timing out, troubleshooting should start with your
internet connection, followed by the above assumptions.

## Getting Started

The python script runs on python3, in a virtual environment to keep the selenium
package isolated (it has a reputation). With python3 installed, make sure you can
access python and pip from the command prompt (i.e. `c:\...\> pip install numpy `)

### Create virtual environment

Install virtualenv: `c:\...\> pip install virtualenv`

navigate the command line to your project:

`cd Users/user-name/Documetents/project`

and create a virtual environment within the project directory:

`virtualenv env`

Before you can install packages or run scripts in your virtual environment, it
needs to activated:

`[path to your project]\env\Scripts\activate`

you should now see the name of your virtual environment to the left of the
command line. Inside your virtual environment, install all the packages you'll
need (pandas, time, and selenium) using `pip install [packagename]`.

### Running AFSA_autoSearch.py

open AFSA_autoSearch.py in your favourite editor and edit the input file path
and extended search options at the top of the script (after the import statements, of course).
Save the changes and run the script from the virtual environment commandline:

`py AFSA_autoSearch.py`
OR
`python3 AFSA_autoSearch.py`
