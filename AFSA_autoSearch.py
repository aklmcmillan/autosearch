import pandas as pd
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from msedge.selenium_tools import Edge, EdgeOptions
#===============================================================================

#load FIDA spreadsheet as csv
FIDAcsv = pd.read_csv(r'C:\path\to\traitstable.csv',
                        encoding= 'unicode_escape')

#search with extended traits keywords?
extended = False

# Define extended keywords
searchTerms = ["life span","maximum body length", "maximum body mass",
                "first maturity", "feeding mode", "sexual differentiation",
                "reproductive mode", "reproduction frequency", "fecundity",
                "larval development", "migration", "dispersal", "sociability",
                "physical protection", "chemical protection", "diel activity"]

# Define output filenames
outfile    = 'SearchCount_fish.csv'
extoutfile = 'extendedSearchCount_fish.csv'
#===============================================================================
# Define AFSA search function
#===============================================================================
def check_exists_by_xpath(xpath, driver):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True
def AFSA_searchCount(spName):
    delay = 5 #seconds
    options = EdgeOptions()
    options.use_chromium = True
    options.add_argument("headless")
    options.add_argument("log-level=3")
    options.add_experimental_option("excludeSwitches",["enable-logging"])
    browser = Edge(executable_path= r'C:\Users\mcmillanan\Documents\FIDA\env\msedgedriver.exe', options = options)
    browser.get('https://www.proquest.com/asfabiological/advanced')

    # ids: textField_, selectAll_SourceType, selectAll_DocumentType, Language_ENG
    try:
        element_present = EC.presence_of_element_located((By.ID, 'queryTermField'))
        WebDriverWait(browser, delay).until(element_present)
    except TimeoutException:
        return "search query timed out"

    searchElem = browser.find_element_by_id('queryTermField')
    searchElem.send_keys(spName)

    try:
        element_present = EC.presence_of_element_located((By.ID, 'selectAll_SourceType'))
        WebDriverWait(browser, delay).until(element_present)
    except TimeoutException:
        return "Search options timed out"

    browser.find_element_by_xpath(".//*[@id='selectAll_SourceType']").click()
    browser.find_element_by_xpath(".//*[@id='selectAll_DocumentType']").click()
    browser.find_element_by_xpath(".//*[@id='Language_ENG']").click()

    searchElem.send_keys(Keys.RETURN)


    try:
        element_present = EC.presence_of_element_located((By.ID, 'pqResultsCount'))
        WebDriverWait(browser, delay).until(element_present)
    except TimeoutException:
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'pagingBoxNoResultContent'))
            WebDriverWait(browser, delay).until(element_present)
        except TimeoutException:
            return "Search results timed out"

    xpath = ".//h1[@id='pqResultsCount']"
    xpath0 = ".//*[@class= 'pagingBoxNoResultContent']"
    if check_exists_by_xpath(xpath, browser):
        resultCount = browser.find_element_by_xpath(xpath).text
    if check_exists_by_xpath(xpath0, browser):
        resultCount = '0'
    browser.quit()
    return resultCount
#===============================================================================
# Start main script

cols = ["species"]
cols.extend(searchTerms)

#select genus and species columns and join them as one species name column
spNames = "\"" + FIDAcsv["Genus"] + " " + FIDAcsv["Species"] + "\""
counts = []
extcounts = []
with open('counts.txt','w') as textfile:
    for spName in spNames:
        c = AFSA_searchCount(spName)
        print(spName + ": " + c)
        if extended:
            if not c in ['0', 'Search query timed out', 'Search options timed out', 'Search results timed out', None]:
                row = [spName]
                for st in searchTerms:
                    newSearch = spName + " AND \"" + st + "\""
                    nc = AFSA_searchCount(newSearch)
                    row.append(nc)
                extcounts.append(row)
                textfile.write(str(row)+'\n')
        counts.append(c)

outdict = {'Search':spNames,
           'Count' :counts}
outdf = pd.DataFrame(outdict)
outdf.to_csv(outfile)
if extended:
    extoutdf = pd.DataFrame(extcounts,columns= cols)
    extoutdf.to_csv(extoutfile)
