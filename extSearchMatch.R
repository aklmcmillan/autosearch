library(tidyverse)
setwd(getSrcDirectory()[1])

tnf <- function(x){
  snames <- names(sdf)[2:17]
  tnames <- names(tdf)[3:19]
  trow <- tdf[which(tdf$spName == x["spName"]),]
  out = FALSE
  for(i in 1:16){
    if(i < 4){
      if((x[snames[i]] > 0 | is.na(x[snames[i]])) & (trow[tnames[i]] == "Not found")){
        out <- TRUE
      }
    }
    if(i == 4){
      if((x[snames[i]] > 0 | is.na(x[snames[i]])) & (trow[tnames[i]] == "Not found" | trow[tnames[i+1]] == "Not found")){
        out <- TRUE
      }
    }
    else{
      if((x[snames[i]] > 0 | is.na(x[snames[i]])) & (trow[tnames[i+1]] == "Not found")){
        out <- TRUE
      }
    }
  }
  return(out)
}

remChar <- function(x) str_remove_all(x, "[a-z ]")

extcsv <- read.csv("extendedSearchCount_fish.csv")
traitscsv <- read.csv("FIDA_FishSpeciesTraits_ForAndrew.csv")

cols <- names(extcsv)[3:length(names(extcsv))]


sdf <- extcsv[,2:18] %>%
  mutate(spName = str_replace_all(.$species,"\"",'')) %>%
  mutate(across(cols, remChar)) %>%
  mutate(across(cols, as.numeric)) %>%
  mutate(cmean = rowMeans(.[cols])) %>%
  filter(cmean > 0 | is.na(cmean))
  

tdf <- traitscsv[ ,c(5,6,8,10,12,14,16,22,24,26,28,30,32,38,40,42,44,46,48)] %>%
  mutate(spName = paste(Genus, Species, sep = " ")) %>%
  filter(paste("\"",spName,"\"",sep='') %in% sdf$species)

sp2search <- sdf[apply(sdf,1,tnf),]
traitdf <- traitscsv %>%
  filter(paste(Genus, Species, sep =" ") %in% sp2search$spName)
write.csv(traitdf,"sp2search.csv")

df <- sp2search[,1:17] %>%
  mutate(resultSum = rowSums(.[cols],na.rm = TRUE))
write.csv(df,"sp2s_counts.csv")
